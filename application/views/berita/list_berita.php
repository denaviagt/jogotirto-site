<div class="navbar-left">
  <button type="button" class="btn btn-success" onclick="loadMenu('<?= base_url('Berita/form_create') ?>')">Tambah Berita</button>
</div>
<div class="title_right">
  <div class="col-md-3 col-sm-3 col-xs-12 navbar-right top_search">
    <div class="input-group">
      <input type="text" name="search" id="search" class="form-control" placeholder="Search for...">
      <span class="input-group-btn">
        <button class="btn btn-default" id="btn-search" type="button">Go!</button>
      </span>
    </div>
  </div>
</div>
<div class="clear-fix"></div>
<br>
<div class="konten-berita">
</div>

<script>
  function loadBerita(url) {
    $.ajax(url, {
      type: 'GET',
      success: function(data, ststus, xhr) {
        var objData = JSON.parse(data);

        $('.konten-berita').html(objData.konten);
      },
      error: function(jqXHR, textStatus, errorMsg) {
        alert('Error : ' + errorMsg)
      }
    })
  }
  loadBerita('<?= base_url() ?>berita/list_berita');

  $('#search').keyup(function() {
    var search = $(this).val();
    if (search != "") {
      cariData(search);
    } else {
      cariData();
    }
  });

  function cariData(query) {
    $.ajax({
      url: '<?= base_url() ?>berita/cari_data',
      type: 'POST',
      data: {
        query: query
      },
      success: function(data, status, xhr) {
        var objData = JSON.parse(data);
        $('.konten-berita').html(objData.konten);
      },
      error: function(jqXHR, textStatus, errorMsg) {
        alert('Error:' + errorMsg);
      }
    });

  }
</script>